#include "Level.h"
#include <SFML/Graphics.hpp>
#include "Wall.h"
#include <iostream>
#include <fstream>
#include "Player.h"



Level::Level()
	:playerInstance(sf::Vector2f(0, 0))
, wallInstance()
{
	wallInstance.push_back(wall(sf::Vector2f(200, 200)));
	wallInstance.push_back(wall(sf::Vector2f(200, 200)));
	wallInstance.push_back(wall(sf::Vector2f(200, 200)));


}
void Level::Level()
{
	playerInstance.Input();
}
void Level::Update(sf::Time FrameTime)
{
	playerInstance.Update(FrameTime);

	for (int i = 0; i < wallInstance.size(); ++i)
	{
		playerInstance.HandleSolidCollision(wallInstance[i].getHitbox());
	}
}

	

void Level ::Input()
{
	playerInstance.Input();

}

void Level::DrawTo(sf::RenderTarget & target)
{
	playerInstance.DrawTo(target);

	for (int i = 0; i < wallInstance.size(); ++i)
	{
		wallInstance[i].DrawTo(target);
	}
}

void Level::LoadLevel(int levelNumber)
{
	//figure ouit the file path based on the levek number 
	std::string filePathPart1 = "Assets/Levels/Level" ;
	std::string filePathPart2 = "1";
	std::string filePathPart3 = ".txt";

	std::string filePath = filePathPart1 + filePathPart2 + filePathPart3;

	std::string filePathPart2 = std::to_string(levelNumber);

	std::string filePath = "Assets/Levels/Level" + std::to_string(levelNumber) + ",txt";

	//open the level file usign this oath 
	std::ifstream inFile;
	inFile.open(filePath);
	if (!inFile)
	{
		std::cerr << "Unable to open file" + filePath;
		exit(1); //call system to stop program with error 
	}

	//clear out the existing level to be ready to read teh new one 
	wallInstance.clear();

	//read through the file, processign:
	//set the starting x and y coordinates used to position level objects
	float x = 0.0f;
	float y = 0.0f;
	//define the spacing we will use for our grid 
	float xSpacing = 100.0f;
	float ySpacing = 100.0f;

	//read each character one by one from the file.....
	char ch;
	while(inFile >> std::noskipws >> ch)
	{
		//perform actions based on what character was read in 
		if (ch == ' ')
		{
			//new column, increase x
			x += xSpacing;
		}
		else if(ch==' \n')
		{
			//new row, increase y and x back to 0 
			y += ySpacing;

			x = 0;
		}
		else if (ch == ' P ')
		{

			//player (positionign the player)
		playerInstance.setPosition(sf::Vector2f(x, y));
		}
		else if (ch == ' W ')
		{
			//walls creating them, psoitioning them, and adding them to the vector
			wallInstance.push_back(wall(sf::Vector2f(x, y)));
		}
		else if (ch == ' _ ')
		{
			//do nothing the empty space 
		}
		else
		{
			//anything else is unrecognised
			std::cerr << "Unrecognised character in level file: " << ch;
		}
	}

	//close the file now taht we arte done with it 
	inFile.close();
}