#pragma once
#include "Player.h"
#include <SFML/Graphics.hpp>
#include <vector>
#include "Wall.h"

class Level
{
public:

	//functions for level.cpp
	void Input();
	void Update(sf::Time frameTime);
	//functions
	void DrawTo(sf::RenderTarget& target);
	void LoadLevel(int levelNumber);


private:

	Player playerInstance;
	std::vector<wall>wallInstance;


};

