#include <SFML/Graphics.hpp>
#include "AssetManager.h"
#include "Player.h"
#include "SpriteObject.h"
#include "AnimatingObject.h"
#include "Wall.h"
#include "Level.h"

int main()
{
	//---------------game setup -----------------------------------------------------
	//sf::RenderWindow window(sf::VideoMode(200, 200), "SFML work !");
	sf::RenderWindow gameWindow;
	gameWindow.create(sf::VideoMode::getDesktopMode(), "maze escape", sf::Style::Titlebar | sf::Style::Close);

	//added code to the set up section 
	Player PlayerObject( sf::Vector2f(30,30));

	wall WallObject(sf::Vector2f(200, 200));

	sf::Sprite testSprite(AssetManager::RequestTexture("Assets/Graphics/PlayerStatic.png"));

	//game clock
	sf::Clock gameClock;

	levelScreen.LoadLevel(1);
	

	while (gameWindow.isOpen())
	{
		sf::Event event;
		while (gameWindow.pollEvent(event))
		{
			if (event.type == sf::Event::Closed) 
			    gameWindow.close();

		}


		

		//---------------update function ---------------

		sf::Time FrameTime = gameClock.restart();

		//player object update 
		PlayerObject.Update(FrameTime);

		PlayerObject.Input();

		PlayerObject.HandleSolidCollision(WallObject.getHitBox());

		gameWindow.clear();
		//Drawn section 
		PlayerObject.DrawTo(gameWindow);
		WallObject.DrawTo(gameWindow);
		gameWindow.display();
	}

	return 0;

	

}
