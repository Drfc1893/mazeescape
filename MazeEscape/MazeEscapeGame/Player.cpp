#include "Player.h"
#include "AssetManager.h"

Player::Player(sf::Vector2f startingPos) :AnimatingObject(AssetManager::RequestTexture("Assets/Graphics/PlayerAnimation.png"), 100, 100, 5.0f)
, velocity(0.0f, 0.0f)
, speed(300.0f)
, previousPosition(startingPos)
{
	
	sprite.setPosition(startingPos);

	AddClip("WalkDown", 0, 3);
	AddClip("WalkRight", 4, 7);
	AddClip("WalkUp", 8, 11);
	AddClip("WalkLeft", 12, 15);

	PlayClip("WalkDown");
	
}

void Player::Input()
{
	//player keybind input
	//start by zeroing out player velocity 
	velocity.x = 0.0f;
	velocity.y = 0.0f;
	bool hasInput = false;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		//move player up 
		velocity.y = -speed;
		if (!hasInput)
		{
			PlayClip("WalkUp");
		}
		hasInput = true;

	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		//move player left 
		velocity.x = -speed;
		if (!hasInput)
		{
			PlayClip("WalkLeft");
		}
		hasInput = true;


	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		//move player down 
		velocity.y = speed;
		if (!hasInput)
		{
			PlayClip("WalkDown");
		}
		hasInput = true;

	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		//move player right 
		velocity.x = speed;
		if (!hasInput)
		{
			PlayClip("WalkRight");
		}
		
		hasInput = true;

	}

	if (!hasInput)
	{
		Stop();
	}

}
void Player::Update(sf::Time FrameTime)
{
	//calculate the new position
	sf::Vector2f newPosition = sprite.getPosition() + velocity * FrameTime.asSeconds();

	//setting position in collision 
	previousPosition = sprite.getPosition();

	//move the player to the new position 
	sprite.setPosition(newPosition);

	AnimatingObject::Update(FrameTime);

	
}

void Player::HandleSolidCollision(sf::FloatRect otherHitbox)
{
	//check if there is actually a collision happenign 
	bool isCollidng = getHitBox().intersects(otherHitbox);

	//if there is a collsion 
	if (isCollidng)
	{
		//TODO:DONE calculate the collisionsdepth (overlap)
		sf::Vector2f depth = CalculateCollisionDepth(otherHitbox);
		//TODO:DONE Determine which is smaller - the x or y overlap
		sf::Vector2f newPosition = sprite.getPosition();
		if (std::abs(depth.x) < std::abs(depth.y))
		{
			//TODO:DONE calculate a new x coordinate such that the object dont overlap
			newPosition.x -= depth.x;
		}
		else
		{
			//TODO:DONE caluculate a new y coordinate such that teh objects don't overlap
			newPosition.y -= depth.y;
		}
		//TODO:DONE move the sprite by the depth in whatever direction was smaller 
		sprite.setPosition(newPosition);
	}
}
