#pragma once
#include "AnimatingObject.h"
#include <cmath>

class Player : public AnimatingObject
{
public: 
	//Constructors / Destructors
	Player(sf::Vector2f startingPos);

	//functions to call Player-specific code 
	void Input();
	void Update(sf::Time frameTime);
	void HandleSolidCollision(sf::FloatRect otherHitbox);

private:

	//data 
	sf::Vector2f velocity;
	float speed;

	//collision code
	sf::Vector2f previousPosition;
	
};

