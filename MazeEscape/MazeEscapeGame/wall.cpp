#include "wall.h"
#include "AssetManager.h"

wall::wall(sf::Vector2f position)
	:SpriteObject(AssetManager::RequestTexture("Assets/Graphics/Wall.png"))
{
	sprite.setPosition(position);
}
